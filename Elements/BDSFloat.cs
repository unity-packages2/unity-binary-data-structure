﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSFloat : BDSBase {
		public float Value;

		public BDSFloat() {

		}

		public BDSFloat(string key, float value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.FLOAT;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadSingle();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
