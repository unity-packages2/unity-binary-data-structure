﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSULong : BDSBase {
		public ulong Value;

		public BDSULong() {

		}

		public BDSULong(string key, ulong value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.ULONG;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadUInt64();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
