﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSByteArray : BDSBase {
		public byte[] Value;

		public BDSByteArray() {

		}

		public BDSByteArray(string key, byte[] value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.BYTEARRAY;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadBytes(binaryReader.ReadInt32());
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value.Length);
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
