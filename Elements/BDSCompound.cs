﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.Collections.Generic;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSCompound : BDSBase {
		public BDSCompound() {

		}

		public BDSCompound(string key) {
			Key = key;
		}

		public Dictionary<string, BDSBase> Value {
			get {
				if (value == null) {
					value = new Dictionary<string, BDSBase>();
				}

				return value;
			}

			protected set {
				this.value = value;
			}
		}

		private Dictionary<string, BDSBase> value;

		public override byte GetType() {
			return BDSType.COMPOUND;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			value = new Dictionary<string, BDSBase>();
			BDSBase bds;

			while ((bds = Read(binaryReader)).GetType() != BDSType.END) {
				value.Add(bds.Key, bds);
			}
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			foreach (KeyValuePair<string, BDSBase> entry in value) {
				Write(entry.Value, binaryWriter);
			}

			Write(new BDSEnd(), binaryWriter);
		}

		public void Add(params BDSBase[] bds) {
			for (int i = 0; i < bds.Length; i++) {
				Value.Add(bds[i].Key, bds[i]);
			}
		}

		public override string ToString() {
			string result = "Compound { ";

			foreach (KeyValuePair<string, BDSBase> entry in value) {
				result += entry.Value.ToString() + ", ";
			}

			result = result.Substring(0, result.Length - 2) + " }";

			return result;
		}
	}
}
