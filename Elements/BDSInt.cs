﻿using Eriju.UnityBinaryDataStructure.Structure;
using System.IO;

namespace Eriju.UnityBinaryDataStructure.Elements {
	public class BDSInt : BDSBase {
		public int Value;

		public BDSInt() {

		}

		public BDSInt(string key, int value) {
			Key = key;
			Value = value;
		}

		public override byte GetType() {
			return BDSType.INT;
		}

		protected override void ReadContents(BinaryReader binaryReader) {
			Value = binaryReader.ReadInt32();
		}

		protected override void WriteContents(BinaryWriter binaryWriter) {
			binaryWriter.Write(Value);
		}

		public override string ToString() {
			return Value.ToString();
		}
	}
}
