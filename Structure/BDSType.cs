﻿using Eriju.UnityBinaryDataStructure.Elements;
using System;

namespace Eriju.UnityBinaryDataStructure.Structure {
	public static class BDSType {
		public const byte END = 0;
		public const byte BOOL = 1;
		public const byte BYTE = 2;
		public const byte SBYTE = 3;
		public const byte CHAR = 4;
		public const byte DECIMAL = 5;
		public const byte DOUBLE = 6;
		public const byte FLOAT = 7;
		public const byte INT = 8;
		public const byte UINT = 9;
		public const byte LONG = 10;
		public const byte ULONG = 11;
		public const byte SHORT = 12;
		public const byte USHORT = 13;
		public const byte STRING = 14;

		public const byte BYTEARRAY = 64;
		public const byte LIST = 65;

		public const byte COMPOUND = 255;

		public static Type GetType(byte typeByte) {
			switch (typeByte) {
				default:
					throw new NotImplementedException($"BDSType {typeByte} is not implemented!");

				case END:
					return typeof(BDSEnd);

				case BOOL:
					return typeof(BDSBool);

				case BYTE:
					return typeof(BDSByte);

				case SBYTE:
					return typeof(BDSSByte);

				case CHAR:
					return typeof(BDSChar);

				case DECIMAL:
					return typeof(BDSDecimal);

				case DOUBLE:
					return typeof(BDSDouble);

				case FLOAT:
					return typeof(BDSFloat);

				case INT:
					return typeof(BDSInt);

				case UINT:
					return typeof(BDSUInt);

				case LONG:
					return typeof(BDSLong);

				case ULONG:
					return typeof(BDSULong);

				case SHORT:
					return typeof(BDSShort);

				case USHORT:
					return typeof(BDSUShort);

				case STRING:
					return typeof(BDSString);

				case BYTEARRAY:
					return typeof(BDSByteArray);

				//case LIST:
				//	return typeof(BDSList);

				case COMPOUND:
					return typeof(BDSCompound);
			}
		}

		public static string GetTypeName(byte type) {
			switch (type) {
				default:
					throw new NotImplementedException($"BDSType {type} is not implemented!");

				case END:
					return "End";

				case BOOL:
					return "Bool";

				case BYTE:
					return "Byte";

				case SBYTE:
					return "SByte";

				case CHAR:
					return "Char";

				case DECIMAL:
					return "Decimal";

				case DOUBLE:
					return "Double";

				case FLOAT:
					return "Float";

				case INT:
					return "Int";

				case UINT:
					return "UInt";

				case LONG:
					return "Long";

				case ULONG:
					return "ULong";

				case SHORT:
					return "Short";

				case USHORT:
					return "UShort";

				case STRING:
					return "String";

				case BYTEARRAY:
					return "ByteArray";

				case LIST:
					return "List";

				case COMPOUND:
					return "Compound";
			}
		}
	}
}
